* Etherpad ESPE
  Etherpad ESPE es un proyecto de código abierto para la
  administración de instancias de etherpad en un sistema distribuido.

* Estructura
  Actualmente el proyecto cuenta con cuatro servidores en maquinas
  virtuales para el sistema de gestión, para la base de datos, para el
  el docker A y para el docker B, los cuales interactuan de la
  siguiente manera:

  #+BEGIN_SRC
                        .----| Etherpad |----.
                       /                      \
Sistema de Gestión |-----------------------------> Base de Datos
  #+END_SRC

** Sistema de Gestión
   El servidor donde se va a instalar el sistema de gestión, el cual
   es la interfaz de usuario mediante la cual se pueden crear
   instancias de etherpad. El sistema de gestión corre por el puerto
   *3000*.

** Base de Datos
   El servidor en el cual se va a guardar todos los datos, para esto
   es necesario tener dos bases de datos: una de mongo y otra de
   mariadb (mysql).

   - mongo :: Almacena los datos del Sistema de Gestión. Corre por el
              puerto *27017*.
   - mariadb :: Almacena los datos generados por el etherpad. Corre
                por el puerto *3306*.

*** Consideraciones
    - Mongo debe tener permisos para acceder remotamente.
    - Mariadb debe tener permisos para acceder remotamente.
    - El firewall debe permitir el acceso a los puertos de las bases
      de datos.

** Etherpad
   El etherpad corre en un contenedor el cual a su vez tiene conexion
   a la base de datos.

* Creación de contenedores
  #+NAME: Contenedor de mariadb
  #+BEGIN_SRC sh
    docker run --name mariadb -e MYSQL_ROOT_PASSWORD=12345 -p 3306:3306 \
    --restart unless-stopped -d mariadb
  #+END_SRC

  #+NAME: Contenedor de Etherpad
  #+BEGIN_SRC sh
    docker run --name etherpad -d --link mariadb:mysql -e \
    ETHERPAD_DB_PASSWORD=12345 -p 9001:9001 --restart unless-stopped \
    tvelocity/etherpad-lite
  #+END_SRC

  #+NAME: Contenedor de mongodb
  #+BEGIN_SRC sh
    docker run --name mongo -p 27017:27017 --restart unless-stopped -d \
    mongo
  #+END_SRC

* Configuraciones
  Las configuraciones se encuentran en
  =controllers/config-servers.js=, en este archivo entre otras cosas
  se guardan las direcciones IP en las cuales se encuentra el sistema
  distribuido.

* Referencias
  - Permitir conexiones a Mariadb,
    http://devdocs.magento.com/guides/v2.0/install-gde/prereq/mysql_remote.html,
  - Configuraciones de Unconplicated Firewall (UFW),
    https://help.ubuntu.com/community/UFW
  - Configurar servicio docker para que corra por tcp,
    https://coreos.com/os/docs/latest/customizing-docker.html
