window.fbAsyncInit = function() {
  FB.init({
    appId      : '225287707972133',
    xfbml      : true,
    version    : 'v2.9'
  });
  FB.AppEvents.logPageView();
};

const FBStatus = {
  REJECT: 0,
  CONNECTED: 1,
  UNAUTHORIZED: 2,
  UNKNOWN: 3,
}

function loginWithFacebook(callback) {
  FB.login(res => {
    console.log('login with facebook', res)
    if (res.status === 'connected')
      checkPermissions(callback)
    else
      callback(FBStatus.REJECT)
  }, { scope: 'public_profile,email', auth_type: 'rerequest' })
}

function checkPermissions(callback) {
  FB.api('/me/permissions', res => {
    console.log('check permissions', res)
    let permissions = res.data[1].status === 'granted'
    callback(permissions ?  FBStatus.CONNECTED : FBStatus.UNAUTHORIZED)
  })
}

function checkLoginStatus(callback) {
  FB.getLoginStatus(res => {
    console.log('check login status', res)
    let connected = res.status === 'connected'
    callback(connected ? FBStatus.CONNECTED : FBStatus.UNKNOWN)
  })
}

function facebookLogin(callback) {
  checkLoginStatus(fbStatus => {
    switch (fbStatus) {
    case FBStatus.CONNECTED:
      checkPermissions(res => {
        if (res === FBStatus.UNAUTHORIZED)
          loginWithFacebook(callback)
        else
          callback(FBStatus.CONNECTED)
      })
      break
    case FBStatus.UNKNOWN:
      loginWithFacebook(callback)
      break
    }
  })
}

function getFacebookToken(callback) {
  FB.getLoginStatus(res => { callback(res) })
}

(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
