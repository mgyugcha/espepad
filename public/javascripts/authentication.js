const restuser = '/rest/users'

function signupController($scope, $http) {

  $scope.disabledButton = false

  $scope.v = {
    password: {
      class: '',
      change: function () {
        let pass = $scope.user.password
        let passRepeat = $scope.user.passwordRepeat
        this.class = !(pass && passRepeat) ? '' :
          (pass === passRepeat) ? 'has-success' : 'has-danger'
      }
    },
    user: {
      class: '',
      change: function () {
        let username = $scope.user.username
	if (!username) {
	  this.class = ''
	  return
	}
        let self = this
        $http.head(`${restuser}/${username}`)
          .then(res => {
	    this.class = res.status === 200 ? 'has-danger' : 'has-success'
          }, res => {
            console.error(res.data)
          })
      }
    }
  }

  $scope.submit = function () {
    if ('has-success' !== $scope.v.password.class ||
        'has-success' !== $scope.v.user.class) {
      notify.formWarning()
      return
    }

    $scope.disabledButton = true

    $http.post(restuser, $scope.user)
      .then(() => {
        notify.saved(() => { window.location = '/login' })
      }, res => {
        notify.failed()
        console.error(res.data)
      })
  }
}

function loginController($scope, $http) {

  $scope.fbLogin = function () {
    facebookLogin(fbStatus => {
      switch (fbStatus) {
      case FBStatus.CONNECTED:
        getFacebookToken(res => {
          console.log(res)
        })
        break
      case FBStatus.UNAUTHORIZED:
        notify.warning('Permiso denegado', `\
Es necesario que Etherpad ESPE tenga acceso a algunos de sus datos`)
        break
      }
    })
  }

  $scope.submit = function () {
    $http.post('/login', $scope.user)
      .then(res => {
        window.location = '/'
      }, res => {
        if (res.status === 400) {
          notify.error('Datos incorrecto', `\
Verifique que el usuario y la contraseña sean correctos`)
        } else {
          notify.serverError()
        }
      })
  }

}

angular.module('authentication', [])
  .controller('signup', signupController)
  .controller('login', loginController)
