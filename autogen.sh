#!/bin/bash

filename='espepad.service'

echo "Creando archivo: $filename"
sed 's,@directory@,'"$PWD"',g' espepad.in > "$filename"
cp "$filename" /etc/systemd/system/"$filename"
echo "systemctl start espepad"
echo "Eliminando espepad.service del directorio del proyecto"
# rm "$filename"
