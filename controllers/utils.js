exports.getProperty = function (data, property, options={}) {
  if (typeof data[property] === 'boolean' || data[property]) {
    if (options.options) {
      if (options.options[data[property]] !== undefined) {
        return data[property]
      } else {
        let err = new Error(`\
La propiedad '${property}' debe tener alguno de los siguientes \
valores: ${JSON.stringify(options.options)}.`)
        err.httpCode = 400
        throw err
      }
    } else {
      return data[property]
    }
  } else {
    if (options.optional) {
      return undefined
    } else {
      let err = new Error(`\
Hubo un error al buscar la propiedad '${property}'`)
      err.httpCode = 400
      throw err
    }
  }
}
