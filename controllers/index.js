const session = require('./session')
const etherpad = require('./config-servers').etherpad
const database = require(__base + '/controllers/database')
const debug = require('debug')('app:controllers:index')

exports.index = function (req, res) {
  let data = { user: req.session.user, navPads: 'active', etherpad }
  res.render('index', data)
}

exports.pad = function (req, res) {
  let data = {
    user: req.session.user, navPads: 'active', pad: req.params.id, etherpad
  }
  res.render('pad', data)
}

exports.config = function (req, res) {
  let data = { user: req.session.user, navConfig: 'active' }
  res.render('config', data)
}

exports.login = {
  get: function (req, res) {
    res.render('login')
  },
  post: function (req, res) {
    session.userLogin(req.body, req.session)
      .then(response => {
        res.status(response ? 200 : 400).end()
      })
      .catch(err => {
        console.error('Login post.', err)
        res.status(500).send(err.message)
      })
  }
}

exports.logout = function (req, res) {
  req.session.destroy(err => {
    if (err) {
      res.send(err)
    } else {
      res.redirect('/login')
    }
  })
}

