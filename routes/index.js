const router = require('express').Router()
const index = require('../controllers/index')

router.get('/', index.index)

router.get('/pad/:id', index.pad)

router.get('/config', index.config)

router.route('/login')
  .get(index.login.get)
  .post(index.login.post)

router.get('/logout', index.logout)

router.get('/signup', (req, res, next) => {
  res.render('signup')
})


module.exports = router;
