const router = require('express').Router()
const get = require(__base + '/controllers/utils').getProperty
const ObjectID = require('mongodb').ObjectID
const database = require(__base + '/controllers/database')
const maincollection = database.collections.PADS

router.route('/')
  .get((req, res) => {
    let query = {
      user_id: new ObjectID(req.session.user._id),
      archived: { $exists: false }
    }
    let collection = database.getCollection(maincollection)
    collection.find(query).toArray()
      .then(docs => {
	res.json(docs)
      })
  })
  .post((req, res) => {
    let pad = req.body
    let collection = database.getCollection(maincollection)
    let schema = {
      title: get(pad, 'title'),
      description: get(pad, 'description'),
      user_id: new ObjectID(req.session.user._id),
      creation: new Date()
    }
    collection.insertOne(schema)
      .then(() => { res.end() })
      .catch(err => {
	console.error(err.message)
	res.status(500).send(err)
      })
  })

router.route('/:id')
  .delete((req, res) => {
    let filter = { '_id': new ObjectID(req.params.id) }
    let update = { $set: { archived: true } }
    let collection = database.getCollection(maincollection)
    collection.update(filter, update)
      .then(() => { res.end() })
      .catch(err => {
	console.error(err.message)
	res.status(500).send(err)
      })
  })

module.exports = router
