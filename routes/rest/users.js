const router = require('express').Router()
const get = require(__base + '/controllers/utils').getProperty
const database = require(__base + '/controllers/database')
const ObjectID = require('mongodb').ObjectID
const maincollection = database.collections.USERS
const debug = require('debug')('app:controllers:users')

router.route('/')
  .post((req, res) => {
    let user = req.body
    let collection = database.getCollection(maincollection)
    let schema = {
      username: get(user, 'username'),
      password: get(user, 'password'),
      name: get(user, 'name'),
      email: get(user, 'email')
    }
    debug('creando usuario %s', user.username)
    collection.insertOne(schema)
      .then(() => { res.end() })
      .catch(err => {
        console.error(err.message)
        res.status(500).send(err)
      })
  })

router.route('/me')
  .get((req, res) => {
    let query = { '_id': new ObjectID(req.session.user._id) }
    let project = { 'password': 0 }
    let collection = database.getCollection(maincollection)
    collection.findOne(query, project)
      .then(doc => {
        res.json(doc)
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })
  .put((req, res) => {
    let filter = { '_id': new ObjectID(req.session.user._id) }
    delete req.body._id
    let update = { $set: req.body }
    let collection = database.getCollection(maincollection)
    collection.update(filter, update)
      .then(() => {
        req.session.user['name'] = req.body.name
        res.end()
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.put('/me/password', (req, res) => {
  let query = {
    '_id': new ObjectID(req.session.user._id),
    'password': req.body.oldPassword
  }
  let collection = database.getCollection(maincollection)
  collection.count(query)
    .then(length => {
      if (length === 0) {
        return null
      } else {
        let update = { $set: { 'password': req.body.password } }
        return collection.update(query, update)
      }
    })
    .then(result => {
      res.status(result === null ? 400 : 200).end()
    })
    .catch(err => {
      console.error(err)
      res.status(500).end()
    })
})

router.route('/:id')
  .head((req, res) => {
    let query = { username: req.params.id }
    let collection = database.getCollection(maincollection)
    collection.count(query)
      .then(length => { res.status(length === 0 ? 204 : 200).end() })
      .catch(err => {
        console.error(err.message)
        res.status(500).send(err.message)
      })
  })

// exports.post = function (req, res) {
//   Users.insert(req.body)
//     .then(() => {
//       res.end()
//     })
//     .catch(err => {
//       console.error(err)
//       res.status(err.httpCode || 500).send(err.message)
//     })
// }

// exports.get = function (req, res) {
//   let query = {
//     '_id': req.params.id
//   }

//   Users.get(query)
//     .then(doc => {
//       res.json(doc)
//     })
//     .catch(err => {
//       console.error(err)
//       res.send(err.message)
//     })
// }

module.exports = router
